<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

// use Com\Tecnick\Barcode\Type\Square\QrCode\Split;

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}


$sql = 'SELECT `nv4_vi_duanmoi_logs`.* , `nv4_users`.`last_name`,`nv4_users`.`first_name`FROM `nv4_vi_duanmoi_logs` JOIN `nv4_users` ON `nv4_vi_duanmoi_logs`.`user_id` = `nv4_users`.`userid` ORDER BY `nv4_vi_duanmoi_logs`.`created_at` DESC;';
$res = $db->query($sql);
$list_log = $res->fetchAll();
// print_r($list_log);die;
$xtpl = new XTemplate('logactiviti.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
//for dữ liệu dự án
foreach($list_log as $key => $value){
    $xtpl->assign('KEY', $key + 1);
    $full_name =$value['last_name'] != null ? $value['last_name'] . '-' . $value['first_name'] : $value['first_name'];
    $xtpl->assign('NGUOI_TAO', $full_name);
    $xtpl->assign('CONTENT', $value['content']);
    $value['created_at'] =  date("H:i d/m/Y", strtotime($value['created_at']));
    $xtpl->assign('NGAY_TAO', $value['created_at']);
    $xtpl->parse('main.loop');

}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';